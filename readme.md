## Category Management Sysmtem

_using laravel framework_

Before you get started, edit your __.env__ and update the following:
__DB_DATABASE:__ categories_management
__DB_USERNAME:__ root
__DB_PASSWORD:__

An example file is also provided (__.env.example__)
_note: create first your database that matches to your DB_DATABASE_

Run the following to get started

- `composer install`
- `php artisan key:generate`
- `php artisan migrate`
- `php artisan db:seed --class=UsersTableSeeder`
- `php artisan serve`

Your application should now be running on __localhost:8080__.
You can access the application using the credentials:

username: `admin_user@test.com`
password: `password1234`

To change host and ports, use command:
- `php artisan serve --host={host} --port={port}`