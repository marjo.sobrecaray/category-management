var Categories = {
    updateId: null,
    create: function (data) {
        var _this = this;

        $.ajax({
            url: '/categories',
            type: 'POST',
            data,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (e) {
                // $('#create-modal').modal('hide');
                $("#create-modal").find('[data-dismiss="modal"]').click()
                _this.get()
                $("#category-data").val('')
            },
            error: function ({ responseText }, textStatus, errorThrown) {
                responseText = typeof responseText  === 'string' ? JSON.parse(responseText) : responseText
                var text = ''
                if (responseText.message && responseText.message === 'CSRF token mismatch.') {
                    text = 'try again'
                }

                if (responseText.category) {
                    text = responseText.category[0]
                }

                $(".alert-danger").html(text);
                $(".alert-danger").removeAttr('hidden');
                $(".alert-danger").show();

                setTimeout( function () {
                    $(".alert-danger").hide();
                }, 3000)
            }
        })
    },

    get: function () {
        $.ajax({
            url: '/categories',
            type: 'GET',
            success: function (e) {
                console.log(e)
                var item = '<a href="/home/'+e.category+'">'
                item += '<li class="row list-group-item" data-id="'+e.id+'">'
                item += '<div class="col-md-12 col-lg-12 px-0">'
                item += '<span class="align-middle category-label">'+e.category+'</span>'
                item += '<button class="btn btn-delete" data-id="'+e.id+'"><i class="fas fa-times"></i></button>'
                item += '<button class="btn btn-edit" data-target="#edit-modal" data-toggle="modal" data-id="'+e.id+'" data="'+e.category+'"><i class="far fa-edit"></i></button>'
                item += '</div>'
                item += '</li></a>'
                $("#category-list").append(item)
            }
        })
    },

    delete: function (id) {
        $.ajax({
            url: '/categories/'+id,
            type: 'DELETE',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (e) {
                $(".main-alert").show()
                $("ul").find("[data-id='" + id + "']").remove();
                setTimeout( function () {
                    $(".main-alert").hide();
                }, 2000)
            }
        })
    },

    update: function (id, data) {
        $.ajax({
            url: '/categories/'+id,
            type: 'PATCH',
            data: { category: data },
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function (e) {
                $("ul").find("[data-id='" + id + "'] .category-label").html($("#edit-category-data").val());
                $("ul").find("[data-id='" + id + "'] .btn-edit").attr('data', $("#edit-category-data").val());
                this.updateId = null
                // $("#edit-modal").modal('hide')
                $("#edit-modal").find('[data-dismiss="modal"]').click()
            },
            error: function ({ responseText }, textStatus, errorThrown) {
                responseText = typeof responseText  === 'string' ? JSON.parse(responseText) : responseText
                var text = ''
                if (responseText.message && responseText.message === 'CSRF token mismatch.') {
                    text = 'try again'
                }

                if (responseText.category) {
                    text = responseText.category[0]
                }

                $(".alert-danger").html(text);
                $(".alert-danger").removeAttr('hidden');
                $(".alert-danger").show();

                setTimeout( function () {
                    $(".alert-danger").hide();
                }, 3000)
            }
        })
    }
}

$(document).ready(function () {

    $("#save-category").on('click', function () {
        Categories.create({ category: $("#category-data").val() });
    })

    $(document).on("click", ".btn-delete", function (e) {
        e.preventDefault()
        Categories.delete($(this).attr('data-id'));
    })

    $(document).on('click', '.btn-edit', function (e) {
        e.preventDefault()
        Categories.updateId = $(this).attr('data-id')
        $("#edit-modal").find('[data-dismiss="modal"]').click()
        $("#edit-category-data").val($(this).attr('data'))
    })

    $(document).on("click", "#edit-category", function () {
        Categories.update(Categories.updateId, $("#edit-category-data").val());
    })

    $(".row").find("[data-toggle='side-nav']").on('click', function () {
        $('.side-nav').toggleClass('toggled')
    });
    //toggle nav
    // $('.btn-category').on('click', function () {
    //     $(".side-nav").addClass('toggled-nav')
    // })
    // $('.btn-close-nav').on('click', function () {
    //     $(".side-nav").removeClass('toggled-nav')
    // })
})
