const $url = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=';
const $infoUrl = 'https://www.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=';
const $apiKey = '6b7e1730b08558bd1860fad6a14fe2bd';
var $limit = 50;

var flickr = {
    pages: 0,
    page: 1,
    searching: false,
    search (item, id, flag = false) {
        var _this = this;
        if (!flag) {
            _this.page = 1
            $('#gallery > .inner').empty()
        }
        $('.toggled-nav').removeClass('toggled-nav')
        $('.spinner').show()
        var test = $url+$apiKey+'&text='+item+'&per_page='+$limit+'&format=json&nojsoncallback=1&content_type=1&sort=relevance&page='+_this.page;
        _this.searching = true;
        this.title = item;
        // $("#category-title").html(this.title+' Photo Gallery')
        $.ajax({
            url: test,
            type: 'GET',
            success: function (response) {
                if(response.stat == "ok"){
                    $('.spinner').hide()
                    _this.searching = false
                    console.log(response.photos)
                    $(".current-page").html(response.photos.page+'/');
                    $(".total-pages").html(response.photos.pages);

                    $.each(response.photos.photo, function( index, value ) {
                        $('#gallery > .inner').append('<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">'+
                                '<a href="/home/'+this.title+'/'+value.id+'" class="thumbnail img-gallery" '+
                                    'data-secret="'+value.secret+'" data-id="'+value.id+'"  title="'+value.title+'">'+
                                    '<img src="https://farm'+value.farm+'.staticflickr.com/'+value.server+'/'+value.id+'_'+value.secret+'.jpg">'+
                                '</a>'+
                            '</div>'
                        );
                    });
                }

                $(".img-gallery").on("click", function () {
                    _this.getInfo($(this).attr('data-id'), $(this).attr('data-secret'))
                })

                $("#gallery").scroll( function (e) {
                    var val1 = $("#gallery").scrollTop() + $("#gallery").height()
                    var val2 = $("#gallery > .inner").height()
                    if(parseInt(val1) >= parseInt(val2)) {
                       if (_this.searching) return
                       _this.page++;
                       _this.search(_this.title, id, true)
                    }
                })
            },
            error: function ({ responseText }, textStatus, errorThrown) {

            }
        })
    },

    getInfo (id, secret) {
        var test = $infoUrl+$apiKey+'&photo_id='+id+'&format=rest&format=json&nojsoncallback=1';
        $.ajax({
            url: test,
            type: 'GET',
            success: function (response) {
                $('.spinner').show()
                if(response.stat == "ok"){
                    $('.spinner').hide()
                    console.log(response)
                    var value = response.photo

                    $("#photo-title").html(value.title._content)
                    $("#photo-description").html(value.description._content)
                    $("#photo-date").html(value.dates.taken)
                    console.log(value.tags)
                    value.tags.tag.forEach(function (val) {
                        $("#photo-tags").append('<div class="badge badge-primary text-wrap mx-1">'+val.raw+'</div>')
                    })
                    $('.the-image').append('<img src="https://farm'+value.farm+'.staticflickr.com/'+value.server+'/'+value.id+'_'+value.secret+'.jpg">')
                }
            },
            error: function ({ responseText }, textStatus, errorThrown) {

            }
        })
    }
}


$(document).ready(function () {
    if ($("#selected-category").val() && !$("#photo-id").val()) {
        flickr.search($("#selected-category").val())
    }
    if ($("#photo-id").val()) {
        flickr.getInfo($("#photo-id").val())
    }
})
