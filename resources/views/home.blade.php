@extends('layout')

@section('content')
<div class="row visible-xs">
  <div class="col-xs-2">
    <button class="btn btn-default" data-toggle="side-nav">
      <i class="glyphicon glyphicon-menu-hamburger"></i>
    </button>
  </div>
  <div class="col-xs-10">
    <h5>@if(isset($selCategory)) {{$selCategory}} @endif Photo Gallery</h5>
  </div>
</div>
<h2 id="category-title" class="text-center py-2 hidden-xs">
  @if(isset($selCategory)) {{$selCategory}} @endif Photo Gallery
</h2>
<div class="row">
  <div class="col-md-3 px-0 side-nav">
    <ul id="category-list" class="list-group">
        @foreach ($categories as $category)
        <a href="/home/{{$category->category}}">
          <li class="row list-group-item @if(isset($selCategory) && $selCategory == $category->category) active @endif" data-id="{{$category->id}}">
              <div class="col-md-12 col-lg-12 px-0">
                <span class="align-middle category-label">{{$category->category}}</span>
                <button class="btn btn-delete" data-id="{{$category->id}}"><i class="fas fa-times"></i></button>
                <button class="btn btn-edit" data-target="#edit-modal" data-toggle="modal" data-id="{{$category->id}}" data="{{$category->category}}"><i class="far fa-edit"></i></button>
              </div>
          </li>
        </a>
        @endforeach
    </ul>
    <button type="button" class="btn btn-primary btn-add" data-toggle="modal" data-target="#create-modal">
      Add<i class="fas fa-plus"></i>
    </button>
  </div>
  <button class="btn-close-nav"><i class="fas fa-angle-left"></i></button>
  @if(isset($selCategory))
  <input type="" name="" value="{{$selCategory}}" hidden id="selected-category">
  @endif
  @if(isset($photo_id))
  <input type="" name="" value="{{$photo_id}}" hidden id="photo-id">
  @endif
  <!--end category last-->

  @if(isset($selCategory) && !isset($photo_id))
  <div class="col-md-9 gallery-container">
      <div class="alert alert-success main-alert" role="alert">
        <i class="fas fa-check"></i> Category Deleted
      </div>

      <div class="container">
        <div class="page-indicator">
          <span class="current-page"></span><span class="total-pages"></span>
        </div>
      </div>
      <div id="gallery">
        <div class="inner row"></div>
      </div>
      <span class="spinner py-4"></span>
  </div>
  @elseif (!isset($selCategory))
  <div class="col-md-9">

    <div class="alert alert-dark" role="alert">
    <span>Select a category...</span></div>
  </div>
  @endif

  @if(isset($selCategory) && isset($photo_id))
  <div class="col-md-9 details">
    <div class="alert alert-success main-alert" role="alert">
      <i class="fas fa-check"></i> Category Deleted
    </div>
    <div class="row py-4 my-4">
      <div class="container visible-xs">
        <a href="/home/{{$selCategory}}" class="btn-back">
          <button class="btn btn-default pull-right">
            <i class="fas fa-angle-left"></i>
          </button>
        </a>
    </div>
      <div class=" col-xs-12 col-md-6 the-image"></div>
      <div class="col-xs-12 col-md-6 the-details ">
        <div class="row hidden-xs">
          <a href="/home/{{$selCategory}}" class="btn-back">
            <button class="btn btn-default pull-right">
              <i class="fas fa-angle-left"></i>
            </button>
          </a>
      </div>
        <div class="alert alert-dark py-0 my-1">
          Title
        </div>
        <h3 id="photo-title"></h3>
        <div class="alert alert-dark py-0 my-1" >
          Description
        </div>
        <div class="text-break" id="photo-description"></div>
        <div class="alert alert-dark py-0 my-1" >
          Tags
        </div>
        <div id="photo-tags">
        </div>
      </div>
    </div>
  </div>
  @endif
</div>
<!-- Modal -->
<div class="modal fade" id="create-modal" tabindex="-1" role="dialog" aria-labelledby="create-modal-title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="create-modal">Create New</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" role="alert"></div>
        <div class="alert alert-success" role="alert"></div>
        <form method="post">
            <div class="form-group">
                <label for="usr">Your Category:</label>
                <input type="text" class="form-control" id="category-data">
                @csrf
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-category">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="create-modal-title" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="create-modal">Edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger" role="alert"></div>
        <div class="alert alert-success" role="alert"></div>
        <form method="post">
            <div class="form-group">
                <label for="usr">Your Category:</label>
                <input type="text" class="form-control" id="edit-category-data">
                @csrf
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="edit-category">Save changes</button>
      </div>
    </div>
  </div>
</div>
<link rel="stylesheet" type="text/css" href="/css/home.css">
<script type="text/javascript" src="/js/home.js"></script>
<script type="text/javascript" src="/js/flickr.js"></script>
@endsection