<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**php artisan db:seed --class=UsersTableSeeder
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Admin User',
            'email' => 'admin_user@test.com',
            'password' => bcrypt('password1234')
        ]);
    }
}
