<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Session;

class LoginController extends Controller
{
    public function index () {
        return view('login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect('/home');
        }
        else {
            if(User::where(['email' => $request->input('email')])->get()->isNotEmpty()) {
                return view('login')
                    ->withErrors(['password' => 'Incorrect Password']);
            }

            return view('login')
                ->withErrors(['email' => 'User not found']);
        }
    }

    public function logout () {
        Auth::logout();
        Session::flush();
        return redirect('/home');
    }
}
