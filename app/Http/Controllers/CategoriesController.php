<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;

class CategoriesController extends Controller
{
    //
    public function create (Request $request) {
        $validator = Validator::make($request->all(), [
            'category' => 'required|unique:categories|max:255',
        ]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->json($error, 422);
        }

        $db = DB::table('categories');
        $db->insert([
            'category' => $request->input('category')
        ]);

        return response()->json(['success' => true]);
    }

    public function get () {
        $data = DB::table('categories')->orderBy('id', 'desc')->first();
        return response()->json($data);
    }

    public function delete ($id) {
        return DB::table('categories')->delete($id);
    }

    public function update (Request $request, $id) {
         $validator = Validator::make($request->all(), [
            'category' => 'required|unique:categories|max:255',
        ]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->json($error, 422);
        }
        return DB::table('categories')->where('id',$id)
            ->update([
                'category' => $request->input('category')
            ]);
    }
}
