<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function index($categoryParam = null, $photoId = null)
    {
        $categories = DB::table('categories')->get();
        return view('home', ['categories' => $categories, 'selCategory' => $categoryParam, 'photo_id' => $photoId ]);
    }
}
